#!/usr/bin/env python
# -- coding: UTF-8 --
from __future__ import absolute_import, unicode_literals

import time

import ujson as json
from tornado.web import RequestHandler

from services import database
from services.modules import autenticate, conexao_mongo, Unauthorized


class ManageWishlist(RequestHandler):

    async def get(self):
        try:
            authorization = self.request.headers.get("Authorization")
            user_data = autenticate(authorization)

            return_datas = self.find_wishlist(user_data['email'])
            self.set_status(200)
            self.write(json.dumps(return_datas))
            await self.finish()

        except Unauthorized as noauth:
            self.set_status(401)
            self.write(json.dumps({"message": str(noauth)}))
            await self.finish()

    async def post(self):
        try:
            authorization = self.request.headers.get("Authorization")
            user_data = autenticate(authorization)

            body = json.loads(self.request.body)

            if body.get("wishlist_name", ""):
                message = self.create_wishlist(user_data["email"], body["wishlist_name"], body)

                if message:
                    self.set_status(200)
                    self.write(json.dumps(message))
                    await self.finish()
                else:
                    self.set_status(202)
                    self.write(json.dumps({"message": "Nao foi possivel criar a lista desejada. Ja existe uma lista com este nome!"}))
                    await self.finish()

            else:
                self.set_status(500)
                self.write(json.dumps({"message": "Houve um erro interno no sistema. Tente novamente mais tarde!"}))
                await self.finish()

        except Unauthorized as noauth:
            self.set_status(401)
            self.write(json.dumps({"message": str(noauth)}))
            await self.finish()

    async def put(self):
        try:
            authorization = self.request.headers.get("Authorization")
            user_data = autenticate(authorization)

            body = json.loads(self.request.body)

            if body.get("wishlist_name", "") and body.get("new_wishlist_name", ""):
                message = self.update_wishlist(user_data["email"], body["wishlist_name"], body["new_wishlist_name"])

                if message:
                    self.set_status(200)
                    self.write(json.dumps(message))
                    await self.finish()
                else:
                    self.set_status(400)
                    self.write(json.dumps({"message": "Nenhuma lista encontrada! Tente mais tarde!"}))
                    await self.finish()

            else:
                self.set_status(500)
                self.write(json.dumps({"message": "Houve um erro interno no sistema. Tente novamente mais tarde!"}))
                await self.finish()

        except Unauthorized as noauth:
            self.set_status(401)
            self.write(json.dumps({"message": str(noauth)}))
            await self.finish()

    async def delete(self):
        try:
            authorization = self.request.headers.get("Authorization")
            user_data = autenticate(authorization)

            body = json.loads(self.request.body)

            if body.get("wishlist_name", ""):
                message = self.remove_wishlist(user_data["email"], body["wishlist_name"])
                self.set_status(200)
                self.write(json.dumps(message))
                await self.finish()
            else:
                self.set_status(500)
                self.write(json.dumps({"message": "Houve um erro interno no sistema. Tente novamente mais tarde!"}))
                await self.finish()

        except Unauthorized as noauth:
            self.set_status(401)
            self.write(json.dumps({"message": str(noauth)}))
            await self.finish()

    async def options(self):
        self.set_status(204)
        await self.finish()

    def find_wishlist(self, email, name=None):

        search = conexao_mongo(database).wishlists.find({
            'email': email,
            'wishlist_name': name if name else {"$exists": True}
        }, {
            '_id': 0
        })
        wish_list = [wish for wish in search if wish]

        return wish_list

    def create_wishlist(self, email, name, query):

        wish_list = self.find_wishlist(email, name)

        if len(wish_list) > 0:
            return {}

        query.update({
            "datetime_insert": int(time.time()),
            "email": email
        })
        conexao_mongo(database).wishlists.insert_one(query)

        return {"message": "Lista de desejo criada com sucesso!"}

    def update_wishlist(self, email, old_name, new_name):

        wish_list = self.find_wishlist(email, old_name)
        if len(wish_list) == 0:
            return {}

        conexao_mongo(database).wishlists.update_one({
            "email": email,
            "wishlist_name": old_name
        }, {
            "$set": {
                "wishlist_name": new_name
            }
        })
        return {"message": "Lista de desejos foi atualizada com sucesso!"}

    def remove_wishlist(self, email, name):

        conexao_mongo(database).wishlists.delete_one({
            "email": email,
            "wishlist_name": name
        })

        return {"message": "Lista de desejos removida com sucesso!"}
