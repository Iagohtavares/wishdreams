#!/usr/bin/env python
# -- coding: UTF-8 --
from __future__ import absolute_import, unicode_literals

import os
import boto3
import base64
import unicodedata
import ujson as json
from uuid import uuid4
from tornado.web import RequestHandler

from services import database
from services.modules import autenticate, conexao_mongo, Unauthorized


class WishlistItems(RequestHandler):

    async def get(self):
        try:
            authorization = self.request.headers.get("Authorization")
            user_data = autenticate(authorization)

            wishlist_name = self.get_argument("wishlist_name", default=None, strip=False)

            if wishlist_name:
                data = self.get_list_items(user_data['email'], wishlist_name)

                if isinstance(data, list):
                    self.set_status(200)
                    self.write(json.dumps(data))
                    await self.finish()
                else:
                    self.set_status(400)
                    self.write(json.dumps(data))
                    await self.finish()

            else:
                self.set_status(500)
                self.write(json.dumps({"message": ""}))
                await self.finish()

        except Unauthorized as noauth:
            self.set_status(401)
            self.write(json.dumps({"message": str(noauth)}))
            await self.finish()

    async def post(self):
        try:
            authorization = self.request.headers.get("Authorization")
            user_data = autenticate(authorization)

            body = json.loads(self.request.body)

            if body.get("item_name", "") and body.get("wishlist_name", ""):

                message = self.add_item(user_data["email"], body)

                if message:
                    self.set_status(200)
                    self.write(json.dumps(message))
                    await self.finish()
                else:
                    self.set_status(400)
                    self.write(json.dumps({"message": "Lista de desejos não encontrada! Tente novamente mais tarde!"}))
                    await self.finish()

            else:
                self.set_status(500)
                self.write(json.dumps({"message": "Houve um erro interno no sistema. Tente novamente mais tarde!"}))
                await self.finish()

        except Unauthorized as noauth:
            self.set_status(401)
            self.write(json.dumps({"message": str(noauth)}))
            await self.finish()

    async def put(self):
        try:
            authorization = self.request.headers.get("Authorization")
            user_data = autenticate(authorization)

            body = json.loads(self.request.body)

            if body.get("item_id", "") and body.get("wishlist_name", ""):
                message = self.update_item(user_data["email"], body)

                if message:
                    self.set_status(200)
                    self.write(json.dumps(message))
                    await self.finish()
                else:
                    self.set_status(400)
                    self.write(json.dumps({"message": "Item e/ou lista de desejo não encontrados. Tente novamente, mais tarde!"}))
                    await self.finish()

            else:
                self.set_status(500)
                self.write(json.dumps({"message": "Houve um erro interno no sistema. Tente novamente mais tarde!"}))
                await self.finish()

        except Unauthorized as noauth:
            self.set_status(401)
            self.write(json.dumps({"message": str(noauth)}))
            await self.finish()

    async def delete(self):
        try:
            authorization = self.request.headers.get("Authorization")
            user_data = autenticate(authorization)

            body = json.loads(self.request.body)

            if body.get("item_id", "") and body.get("wishlist_name", ""):
                message = self.delete_item(user_data["email"], body)

                if message:
                    self.set_status(200)
                    self.write(json.dumps(message))
                    await self.finish()
                else:
                    self.set_status(400)
                    self.write(json.dumps({"message": "Lista de desejo não encontrada!"}))
                    await self.finish()

            else:
                self.set_status(500)
                self.write(json.dumps({"message": "Houve um erro interno no sistema. Tente novamente mais tarde!"}))
                await self.finish()

        except Unauthorized as noauth:
            self.set_status(401)
            self.write(json.dumps({"message": str(noauth)}))
            await self.finish()

    async def options(self):
        self.set_status(204)
        await self.finish()

    def get_list_items(self, email, wishlist_name):

        mongo_return = conexao_mongo(database).wishlists.find_one({
            "email": email,
            "wishlist_name": wishlist_name
        })

        if not mongo_return:
            return {"message": "Lista de desejo não encontrada. Tente novamente mais tarde!"}

        items = [item for item in mongo_return.get('items', [])]
        return items

    def add_item(self, email, body):

        raw_image_str, image_bytes = self.format_picture(email, body["wishlist_name"], body.get("picture", ""))

        data_wishlist = conexao_mongo(database).wishlists.find_one({
            "email": email,
            "wishlist_name": body["wishlist_name"]
        })

        if not data_wishlist:
            return {}

        item = {
            "item_id": str(uuid4().hex),
            "item_name": body["item_name"],
            "description": body.get("description", ""),
            "item_link": body.get("item_link", ""),
            "picture": image_bytes,
            "raw_picture": raw_image_str,
            "item_buyed": body.get("item_buyed", False),
            "favorite":  body.get("favorite", False)
        }

        conexao_mongo(database).wishlists.update_one({
            "email": email,
            "wishlist_name": body["wishlist_name"]
        }, {
            "$push": {
                "items": item
            }
        })

        return {"message": "Item adicionado com sucesso!"}

    def update_item(self, email, body):

        raw_image_str, image_bytes = self.format_picture(email, body["wishlist_name"], body.get("picture", ""))

        data_wishlist = conexao_mongo(database).wishlists.find_one({
            "email": email,
            "wishlist_name": body["wishlist_name"]
        })

        if not data_wishlist:
            return {}

        if len(data_wishlist.get("items", [])) == 0:
            return {}

        flag = False
        for item in data_wishlist.get("items", []):
            if str(item.get("item_id", "")) == str(body["item_id"]):
                item.update({
                    "item_name": body["item_name"] if body.get("item_name", "") else item["item_name"],
                    "description": body["item_name"] if body.get("description", "") else item["description"],
                    "item_link": body["item_link"] if body.get("item_link", "") else item["item_link"],
                    "picture": image_bytes if image_bytes else item["picture"],
                    "raw_picture": raw_image_str if raw_image_str else item["raw_picture"],
                    "item_buyed": body["item_buyed"] if body.get("item_buyed", "") else item["item_buyed"],
                    "favorite":  body["favorite"] if body.get("item_buyed", "") else item["favorite"]
                })
                flag = True

        if not flag:
            return {}

        conexao_mongo(database).wishlists.update_one({
            "email": email,
            "wishlist_name": body["wishlist_name"]
        }, {
            "$set": {
                "items": data_wishlist.get("items", [])
            }
        })

        return {"message": "Item atualizado com sucesso!"}

    def delete_item(self, email, body):

        wish_list_name = body.pop("wishlist_name")

        data_wishlist = conexao_mongo(database).wishlists.find_one({
            "email": email,
            "wishlist_name": wish_list_name
        })

        if len(data_wishlist["items"]) == 0:
            return {}

        conexao_mongo(database).wishlists.update_one({
            "email": email,
            "wishlist_name": wish_list_name
        }, {
            "$pull": {
                "items": body["item_id"]
            }
        })

        return {"message": "Item removido com sucesso"}

    def format_picture(self, email, wish_list_name, base64_img_str):

        if not base64_img_str:
            return '', ''

        filename = str(uuid4().hex)
        filepath = f'/tmp/{filename}'
        path = f"{email}/{wish_list_name}/"

        with open(filepath, 'wb') as file:
            base64_img_bytes = base64_img_str.split(',')[1].encode('utf-8')
            image_data = base64.decodebytes(base64_img_bytes)
            file.write(image_data)

        # s3_key = ''.join(self.send_data_S3(access_key, secret_key, bucket, file, path, region))
        os.unlink(filepath)

        return base64_img_str, file

    # def send_data_S3(self, AK, SK, bucket, file, path, region):
    #
    #     cliente_S3 = boto3.client('s3', aws_access_key_id=AK, aws_secret_access_key=SK, region_name=region)
    #
    #     if not isinstance(file, list):
    #         file = [file]
    #
    #     link_list = []
    #     for fil in file:
    #
    #         if path is not None:
    #             caminho_destino = os.path.join(path, os.path.basename(fil))
    #         else:
    #             caminho_destino = os.path.basename(fil)
    #         cliente_S3.upload_file(fil, bucket, caminho_destino)
    #
    #         link_list.append(
    #             cliente_S3.generate_presigned_url(
    #                 'get_object', {
    #                     'Bucket': bucket,
    #                     'Key': self.normaliza_acentos(caminho_destino)
    #                 }
    #             )
    #         )
    #
    #     if len(link_list) == 1:
    #         return link_list[0]
    #     else:
    #         return link_list
    #
    # def normaliza_acentos(self, string_entrada, encoding_entrada='UTF-8', esquema='NFKD'):
    #
    #     if isinstance(string_entrada, str):
    #         entrada_unicode = string_entrada
    #     elif isinstance(string_entrada, str):
    #         entrada_unicode = str(string_entrada, encoding_entrada)
    #     else:
    #         raise TypeError("A entrada deve ser uma String str ou unicode!")
    #
    #     entrada_normalizada = unicodedata.normalize(esquema, entrada_unicode).encode('ASCII', 'ignore')
    #
    #     saida_normalizada = "{}".format(entrada_normalizada)
    #     if saida_normalizada.startswith("b'") and saida_normalizada.endswith("'"):
    #         saida_normalizada = saida_normalizada[2:-1]
    #     elif saida_normalizada.startswith('b"') and saida_normalizada.endswith('"'):
    #         saida_normalizada = saida_normalizada[2:-1]
    #
    #     return saida_normalizada