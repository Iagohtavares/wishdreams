#!/usr/bin/env python
# -- coding: UTF-8 --
from __future__ import absolute_import, unicode_literals

import os
import random

import boto3
import base64
import unicodedata
import ujson as json
from uuid import uuid4
from tornado.web import RequestHandler

from services import database
from services.modules import autenticate, conexao_mongo, Unauthorized


class WishRandomItem(RequestHandler):

    async def get(self):
        try:
            authorization = self.request.headers.get("Authorization")
            user_data = autenticate(authorization)

            wishlist_name = self.get_argument("wishlist_name", default=None, strip=False)

            data = self.get_random_item(user_data['email'], wishlist_name)

            if data:
                self.set_status(200)
                self.write(json.dumps(data))
                await self.finish()
            else:
                self.set_status(202)
                self.write(json.dumps({"message": "Não há itens cadastrados"}))
                await self.finish()

        except Unauthorized as noauth:
            self.set_status(401)
            self.write(json.dumps({"message": str(noauth)}))
            await self.finish()

    async def options(self):
        self.set_status(204)
        await self.finish()

    def get_random_item(self, email, wishlist_name):

        mongo_return = conexao_mongo(database).wishlists.find({
            "email": email,
            "wishlist_name": wishlist_name if wishlist_name else {"$exists": True}
        })
        mongo_return = list(mongo_return)

        if len(mongo_return) == 0:
            return {}

        items = [
            item
            for datas in mongo_return
            for item in datas.get("items", [])
            if item
        ]

        length = len(items)
        if length == 0:
            return {}

        index_random_item = int(random.randrange(0, length))
        return items[index_random_item]
