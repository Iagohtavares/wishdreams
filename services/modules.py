import jwt
from pymongo import MongoClient

from services.user import private_key
from services import database, network
from services.tests.exceptions import Unauthorized


def autenticate(authorization):

    if not authorization:
        raise Unauthorized("No header authorization.")

    format_token, token = authorization.split(" ")
    if not token:
        raise Unauthorized("Unknown token!")

    try:
        decode_jwt = jwt.decode(token, private_key, algorithms=["HS256"], options={"verify_signature": True})
    except jwt.ExpiredSignatureError:
        raise Unauthorized("Expired token!")
    except Exception as exp:
        print(exp)

    user_data = conexao_mongo(database).user_tokens.find_one({
        "token": token
    })

    if not user_data:
        raise Unauthorized("Expired token!")

    if not user_data["login"]:
        raise Unauthorized("Invalid token!")

    return user_data


def conexao_mongo(database):
    client = MongoClient(network)[database]
    return client