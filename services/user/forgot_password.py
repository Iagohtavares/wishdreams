#!/usr/bin/env python
# -- coding: UTF-8 --
from __future__ import absolute_import, unicode_literals

import random

import sendgrid
import ujson as json
from tornado.web import RequestHandler

from services import database
from services.user import api_key_sendgrid
from services.modules import conexao_mongo


class ForgotPassword(RequestHandler):

    async def post(self):

        body = json.loads(self.request.body)

        if body.get("email", ""):
            response = self.check_user(body["email"])

            if response:
                self.set_status(200)
                self.write(json.dumps(response))
                await self.finish()
            else:
                self.set_status(400)
                self.write(json.dumps({"message": "Usuário não encontrado. Tente novamente mais tarde!"}))
                await self.finish()
        else:
            self.set_status(500)
            self.write({"message": "Email invalido"})
            await self.finish()

    async def options(self):
        self.set_status(204)
        await self.finish()

    def check_user(self, email):
        user = conexao_mongo(database).users.find_one({
            'email': email
        })

        if not user:
            return {}

        reset_token = random.randrange(1000000, 9999999)

        conexao_mongo(database).users.update_one({
            "_id": user["_id"]
        }, {
            "$set": {
                "reset_token": reset_token
            }
        })

        ''' 
        O correto seria enviar este token para o email do usuario via sendgrid, como não possuimos uma api_key valida
        retornaremos via response para fins de teste de funcionalidade
        '''
        return {"reset_token": reset_token}

    def create_connection_sendgrid(self):
        try:
            connection_sendgrid = sendgrid.SendGridAPIClient(api_key=api_key_sendgrid)
            return connection_sendgrid
        except Exception as exp:
            print(exp)

    def send_email_sendgrid(self, recipients: list, subject: str, message: str, senders: str, html=False):
        connection_sendgrid = self.create_connection_sendgrid()

        if not isinstance(recipients, list):
            recipients = [recipients]

        message_email = {
            "from": {"email": senders},
            "personalizations": [{
                "to": [{"email": recip} for recip in recipients],
                "subject": subject
            }],
            "content": [{
                "type": "text/html" if html else "text/plain",
                "value": message
            }]
        }

        return connection_sendgrid.client.mail.send.post(request_body=message_email)
