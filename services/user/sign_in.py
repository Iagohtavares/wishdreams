#!/usr/bin/env python
# -*- coding: UTF-8 -*-
from __future__ import absolute_import, unicode_literals

import jwt
import time
import hashlib
import datetime
import ujson as json
from tornado.web import RequestHandler

from services import database
from services.user import private_key
from services.modules import conexao_mongo


class SignIn(RequestHandler):
    async def post(self):
        body = json.loads(self.request.body)

        if body.get("email", "") and body.get("password", ""):
            response = self.login(body)

            if response:
                self.set_status(200)
                self.write(json.dumps(response))
                await self.finish()
            else:
                self.set_status(400)
                self.write(json.dumps({"message": "Login e/ou senha incorretos. Tente novamente!"}))
                await self.finish()

        else:
            self.set_status(500)
            self.write({"message": "Campos de login e/ou senha invalidos"})
            await self.finish()

    async def options(self):
        self.set_status(204)
        await self.finish()

    def login(self, body):
        search_data = conexao_mongo(database).users.find_one({
            "email": body["email"],
            "password": hashlib.md5(body["password"].encode()).hexdigest()
        })

        if not search_data:
            return {}
        search_data["id"] = str(search_data.pop("_id"))

        payload_encoded = {
            "payload": search_data,
            "exp": datetime.datetime.utcnow() + datetime.timedelta(hours=1)
        }

        token = jwt.encode(payload_encoded, private_key, algorithm="HS256")
        conexao_mongo(database).user_tokens.insert_one({
            "token": token,
            "email": body["email"],
            "datahora_int": int(time.time()),
            "login": True,
            "id": search_data["id"]
        })

        return {
            **search_data,
            "token": token,
        }
