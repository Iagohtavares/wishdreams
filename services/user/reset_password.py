#!/usr/bin/env python
# -- coding: UTF-8 --
from __future__ import absolute_import, unicode_literals

import hashlib
import ujson as json
from tornado.web import RequestHandler

from services import database
from services.modules import conexao_mongo


class ResetPassword(RequestHandler):

    async def post(self):

        body = json.loads(self.request.body)

        if body.get("email", "") and body.get("reset_token", "") and body.get("new_password", ""):
            message = self.reset(body)

            if message:
                self.set_status(200)
                self.write(json.dumps(message))
                await self.finish()
            else:
                self.set_status(400)
                self.write(json.dumps({"message": "Usuário não encontrado. Tente novamente mais tarde!"}))
                await self.finish()

        else:
            self.set_status(500)
            self.write({"message": "Campos de login e/ou senha e/ou reset token invalidos"})
            await self.finish()

    async def options(self):
        self.set_status(204)
        await self.finish()

    def reset(self, body):
        check = conexao_mongo(database).users.find_one({
            "email": body["email"],
            "reset_token": body["reset_token"]
        })

        if not check:
            return {}

        conexao_mongo(database).users.update({
            "email": body["email"],
        }, {
            "$set": {
                "reset_token": False,
                "password": hashlib.md5(body["new_password"].encode()).hexdigest()
            }
        })
        return {"message": "Nova senha definida e salva"}
