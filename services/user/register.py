#!/usr/bin/env python
# -- coding: UTF-8 --
from __future__ import absolute_import, unicode_literals

import time
import hashlib
import ujson as json
from bson import ObjectId
from tornado.web import RequestHandler

from services import database
from services.modules import autenticate, conexao_mongo, Unauthorized


class Register(RequestHandler):

    async def get(self):
        try:
            authorization = self.request.headers.get("Authorization")
            user_data = autenticate(authorization)

            user = self.find_registry(user_data)
            if user:
                self.set_status(200)
                self.write(json.dumps(user))
                await self.finish()
            else:
                self.set_status(500)
                self.write(json.dumps({"message": "Houve um erro interno no sistema. Tente novamente mais tarde!"}))
                await self.finish()

        except Unauthorized as noauth:
            self.set_status(401)
            self.write(json.dumps({"message": str(noauth)}))
            await self.finish()

    async def post(self):
        body = json.loads(self.request.body)

        if body.get("email", "") and body.get("password", ""):
            message = self.insert_registry(body)
            if message:
                self.set_status(200)
                self.write(json.dumps(message))
                await self.finish()
            else:
                self.set_status(202)
                self.write(json.dumps({"message": "Usuario ja possui cadastrado!"}))
                await self.finish()
        else:
            self.set_status(500)
            self.write(json.dumps({"message": "Houve um erro interno no sistema. Tente novamente mais tarde!"}))
            await self.finish()

    async def put(self):
        try:
            authorization = self.request.headers.get("Authorization")
            user_data = autenticate(authorization)

            body = json.loads(self.request.body)

            if body.get("email", "") and body.get("password", ""):
                message = self.update_registry(user_data, body)

                if message:
                    self.set_status(200)
                    self.write(json.dumps(message))
                    await self.finish()
                else:
                    self.set_status(400)
                    self.write(json.dumps({"message": "Dados invalidos. Tente novamente mais tarde!"}))
                    await self.finish()

            else:
                self.set_status(500)
                self.write(json.dumps({"message": "Houve um erro interno no sistema. Tente novamente mais tarde!"}))
                await self.finish()

        except Unauthorized as noauth:
            self.set_status(401)
            self.write(json.dumps({"message": str(noauth)}))
            await self.finish()

    async def delete(self):
        try:
            authorization = self.request.headers.get("Authorization")
            user_data = autenticate(authorization)

            body = json.loads(self.request.body)

            if body.get("email", "") and body.get("password", ""):
                user = conexao_mongo(database).users.find_one({
                    "_id": ObjectId(user_data["id"]),
                    "email": body['email'],
                    "password": hashlib.md5(body["password"].encode()).hexdigest()
                })

                if user:
                    conexao_mongo(database).users.delete_one({
                        "_id": ObjectId(user_data["id"])
                    })
                    self.set_status(200)
                    self.write(json.dumps({"message": "Usuario removido com suceeso!"}))
                    await self.finish()

                else:
                    self.set_status(400)
                    self.write(json.dumps({"message": "Usuario não encontrado! Tente novamente mais tarde."}))
                    await self.finish()

            else:
                self.set_status(500)
                self.write(json.dumps({"message": "Houve um erro interno no sistema. Tente novamente mais tarde!"}))
                await self.finish()

        except Unauthorized as noauth:
            self.set_status(401)
            self.write(json.dumps({"message": str(noauth)}))
            await self.finish()

    async def options(self):
        self.set_status(204)
        await self.finish()

    def find_registry(self, user_data):
        user = conexao_mongo(database).users.find_one({
            "_id": ObjectId(user_data["id"])
        }, {
            "_id": 0,
            "password": 0,
        })
        if not user:
            return {}
        return user

    def insert_registry(self, query):
        registry = self.check_registry(query)
        if registry:
            return {}

        query.update({
            "datetime_insert": int(time.time()),
            "password": hashlib.md5(query["password"].encode()).hexdigest()
        })
        conexao_mongo(database).users.insert_one(query)

        return {"message": "Usuario cadastrado com sucesso!"}

    def update_registry(self, user_data, query):

        password = hashlib.md5(query['password'].encode()).hexdigest()

        identify = conexao_mongo(database).users.find_one({
            "_id": ObjectId(user_data["id"]),
            "email": query["email"],
            "password": password
        })

        if not identify:
            return {}

        flag = False
        if query.get("new_password", ""):
            new_password = hashlib.md5(query["new_password"].encode()).hexdigest()
            flag = True

        query.update({
            "datetime_update": int(time.time()),
            "password": new_password if flag else password,
            "email": query.pop("new_email") if query.get("new_email", "") else query["email"]
        })

        conexao_mongo(database).users.find_one_and_update({
            "_id": ObjectId(user_data["id"])
        }, {
            "$set": query
        })

        return {"message": "Usuario atualizado com sucesso!"}

    def check_registry(self, query):
        user = conexao_mongo(database).users.find_one({
            'email': query['email']
        })

        if user:
            return True
        return False
