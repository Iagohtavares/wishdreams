#!/usr/bin/env python
# -- coding: UTF-8 --
from __future__ import absolute_import, unicode_literals

import ujson as json
from tornado.web import RequestHandler

from services import database
from services.modules import autenticate, conexao_mongo, Unauthorized


class Logout(RequestHandler):

    async def post(self):
        try:
            authorization = self.request.headers.get("Authorization")
            user_data = autenticate(authorization)

            body = json.loads(self.request.body)

            if body.get("email", ""):
                message = self.logout_user(body["email"], user_data)

                if message:
                    self.set_status(200)
                    self.write(json.dumps(message))
                    await self.finish()
                else:
                    self.set_status(400)
                    self.write(json.dumps({"message": "Usuario não encontrado! Tente novamente mais tarde."}))
                    await self.finish()
            else:
                self.set_status(500)
                self.write({"message": "Houve um erro interno no sistema. Tente novamente mais tarde!"})
                await self.finish()

        except Unauthorized as noauth:
            self.set_status(401)
            self.write(json.dumps({"message": str(noauth)}))
            await self.finish()

    async def options(self):
        self.set_status(204)
        await self.finish()

    def logout_user(self, email, user_data):

        user = conexao_mongo(database).users.find_one({
            "email": email
        })

        if not user:
            return {}
        user['id'] = str(user.pop('_id'))

        if user['id'] != user_data['id']:
            return {}

        conexao_mongo(database).user_tokens.delete_one({
            "token": user_data["token"]
        })

        return {"message": "Logout realizado com sucesso. Volte mais vezes!"}
