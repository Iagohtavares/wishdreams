import json
import requests
import unittest

url = "http://localhost:3000/api/manage_wishlist/"


class TestGetWishlist(unittest.TestCase):

    def test_successful_get_wishlist(self):
        body = {
            "email": "test_sucesseful_added_wishlist@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post(url, data=json.dumps(body), headers=header)

        response = requests.get(url, data=json.dumps({}), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_get_wishlist_without_header(self):
        body = {
            "email": "test_get_wishlist_without_header@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post(url, data=json.dumps(body), headers=header)

        response = requests.get(url, data=json.dumps({}))
        self.assertEqual(response.status_code, 401)
