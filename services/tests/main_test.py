import unittest

from services.tests.test_create_user import TestCreateUser
from services.tests.test_delete_current_user import TestDeleteCurrentUser
from services.tests.test_forgot_password import TestForgotPassword
from services.tests.test_get_current_user import TestGetCurrentUser
from services.tests.test_login import TestLogin
from services.tests.test_logout import TestLogout
from services.tests.test_reset_password import TestResetPassword
from services.tests.test_update_current_user import TestUpdateCurrentUser

from services.tests.test_add_wishlist import TestAddWishlist
from services.tests.test_get_wishlist import TestGetWishlist
from services.tests.test_update_wishlist import TestUpdateWishlist
from services.tests.test_delete_wishlist import TestDeleteWishlist

from services.tests.test_add_item_wishlist import TestAddItemWishlist
from services.tests.test_get_items_wishlist import TestGetItemsWishlist
from services.tests.test_update_item_wishlist import TestUpdateItemWishlist
from services.tests.test_delete_item_wishlist import TestDeleteItemWishlist

from services.tests.test_get_random_item_wishlist import TestGetRandomItemsWishlist

if __name__ == '__main__':
    unittest.main()
