import json
import requests
import unittest

url = "http://localhost:3000/api/user/register/"


class TestCreateUser(unittest.TestCase):

    def test_create_a_user(self):
        body = {
            "email": "test_create_a_user@test.com",
            "password": "12345",
        }
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 200)

    def test_create_existing_user(self):
        body = {
            "email": "test_create_existing_user@test.com",
            "password": "12345",
        }
        _ = requests.post(url, data=json.dumps(body))
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 202)

    def test_create_user_without_email(self):
        body = {
            "password": "12345",
        }
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 500)

    def test_create_user_without_password(self):
        body = {
            "email": "test_create_user_without_password@test.com",
        }
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 500)

    def test_create_a_new_user_with_additional_fields(self):
        body = {
            "email": "test_create_a_new_user_with_additional_fields@test.com",
            "password": "12345",
            "New_field1": "Things1",
            "New_field2": [],
            "New_field3": {}
        }
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 200)
