import json
import requests
import unittest

url = "http://localhost:3000/api/user/reset_password/"


class TestResetPassword(unittest.TestCase):

    def test_successful_reset_password(self):
        body = {
            "email": "test_sucesseful_reset_password@test.com",
            "password": "1234",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()
        _ = requests.post(
            "http://localhost:3000/api/user/logout/",
            data=json.dumps({"email": body["email"]}),
            headers={"authorization": f"Bearer {response_login['token']}"}
        )

        body = {
            "email": "test_sucesseful_reset_password@test.com",
        }
        response_forgot = requests.post("http://localhost:3000/api/user/forgot_password/", data=json.dumps(body))
        response_forgot = response_forgot.json()

        body = {
            "email": "test_sucesseful_reset_password@test.com",
            "reset_token": response_forgot["reset_token"],
            "new_password": "12345"
        }
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 200)

    def test_reset_password_without_reset_token(self):
        body = {
            "email": "test_reset_password_without_reset_token@test.com",
            "password": "1234",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()
        _ = requests.post(
            "http://localhost:3000/api/user/logout/",
            data=json.dumps({"email": body["email"]}),
            headers={"authorization": f"Bearer {response_login['token']}"}
        )

        body = {
            "email": "test_reset_password_without_reset_token@test.com",
            "new_password": "12345"
        }
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 500)

    def test_reset_password_without_new_password(self):
        body = {
            "email": "test_reset_password_without_new_password@test.com",
            "password": "1234",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()
        _ = requests.post(
            "http://localhost:3000/api/user/logout/",
            data=json.dumps({"email": body["email"]}),
            headers={"authorization": f"Bearer {response_login['token']}"}
        )

        body = {
            "email": "test_reset_password_without_new_password@test.com",
        }
        response_forgot = requests.post("http://localhost:3000/api/user/forgot_password/", data=json.dumps(body))
        response_forgot = response_forgot.json()

        body = {
            "email": "test_reset_password_without_new_password@test.com",
            "reset_token": response_forgot["reset_token"],
        }
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 500)

    def test_reset_password_without_email(self):
        body = {
            "email": "test_reset_password_without_email@test.com",
            "password": "1234",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()
        _ = requests.post(
            "http://localhost:3000/api/user/logout/",
            data=json.dumps({"email": body["email"]}),
            headers={"authorization": f"Bearer {response_login['token']}"}
        )

        body = {
            "email": "test_reset_password_without_email@test.com",
        }
        response_forgot = requests.post("http://localhost:3000/api/user/forgot_password/", data=json.dumps(body))
        response_forgot = response_forgot.json()

        body = {
            "reset_token": response_forgot["reset_token"],
            "new_password": "12345"
        }
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 500)

    def test_reset_password_with_a_wrong_email(self):
        body = {
            "email": "test_reset_password_with_a_wrong_email@test.com",
            "password": "1234",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()
        _ = requests.post(
            "http://localhost:3000/api/user/logout/",
            data=json.dumps({"email": body["email"]}),
            headers={"authorization": f"Bearer {response_login['token']}"}
        )

        body = {
            "email": "test_reset_password_with_a_wrong_email@test.com",
        }
        response_forgot = requests.post("http://localhost:3000/api/user/forgot_password/", data=json.dumps(body))
        response_forgot = response_forgot.json()

        body = {
            "email": "test_reset_password_with_a_wrong_email_11111@test.com",
            "reset_token": response_forgot["reset_token"],
            "new_password": "12345"
        }
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 400)

    def test_reset_password_with_additional_fields(self):
        body = {
            "email": "test_reset_password_with_additional_fields@test.com",
            "password": "1234",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()
        _ = requests.post(
            "http://localhost:3000/api/user/logout/",
            data=json.dumps({"email": body["email"]}),
            headers={"authorization": f"Bearer {response_login['token']}"}
        )

        body = {
            "email": "test_reset_password_with_additional_fields@test.com",
        }
        response_forgot = requests.post("http://localhost:3000/api/user/forgot_password/", data=json.dumps(body))
        response_forgot = response_forgot.json()

        body = {
            "email": "test_reset_password_with_additional_fields@test.com",
            "reset_token": response_forgot["reset_token"],
            "new_password": "12345",
            "New_field1": "Things1",
            "New_field2": [],
            "New_field3": {}
        }
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
