import json
import requests
import unittest

url = "http://localhost:3000/api/manage_wishlist/"


class TestUpdateWishlist(unittest.TestCase):

    def test_successful_update_wishlist(self):
        body = {
            "email": "test_sucesseful_added_wishlist@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post(url, data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 1",
            "new_wishlist_name": "House List 2"
        }
        response = requests.put(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_update_wishlist_without_header(self):
        body = {
            "email": "test_update_wishlist_without_header@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post(url, data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 1",
            "new_wishlist_name": "House List 2"
        }
        response = requests.put(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 401)

    def test_update_wishlist_without_wishlist_name(self):
        body = {
            "email": "test_update_wishlist_without_wishlist_name@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post(url, data=json.dumps(body), headers=header)

        body = {
            "new_wishlist_name": "House List 2"
        }
        response = requests.put(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 500)

    def test_update_wishlist_without_new_wishlist_name(self):
        body = {
            "email": "test_update_wishlist_with_a_non_existing_new_wishlist_name@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post(url, data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 1",
        }
        response = requests.put(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 500)

    def test_update_wishlist_with_a_non_existing_wishlist_name(self):
        body = {
            "email": "test_update_wishlist_with_a_non_existing_wishlist_name@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post(url, data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 200",
            "new_wishlist_name": "House List 3"
        }
        response = requests.put(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 400)

    def test_update_wishlist_with_additional_fields(self):
        body = {
            "email": "test_sucesseful_added_wishlist@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post(url, data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 1",
            "new_wishlist_name": "House List 2",
            "New_field1": "Things1",
            "New_field2": [],
            "New_field3": {}
        }
        response = requests.put(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 200)
