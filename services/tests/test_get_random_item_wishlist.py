import json
import requests
import unittest

url = "http://localhost:3000/api/wishlist_random_item/"


class TestGetRandomItemsWishlist(unittest.TestCase):

    def test_successful_get_random_item_wishlist(self):
        body = {
            "email": "test_successful_get_random_item_wishlist@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post("http://localhost:3000/api/manage_wishlist/", data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 1",
            "item_name": "Hat",
            "description": "A red hat",
            "item_link": "www.amazon.com.br/red_hat",
            "picture": ""
        }
        _ = requests.post("http://localhost:3000/api/wishlist_items/", data=json.dumps(body), headers=header)

        url_params = url+f"?wishlist_name={body['wishlist_name']}"
        response = requests.get(url_params, data=json.dumps({}), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_get_random_item_wishlist_without_params(self):
        body = {
            "email": "test_get_items_wishlist_without_params@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post("http://localhost:3000/api/manage_wishlist/", data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 1",
            "item_name": "Hat",
            "description": "A red hat",
            "item_link": "www.amazon.com.br/red_hat",
            "picture": ""
        }
        _ = requests.post("http://localhost:3000/api/wishlist_items/", data=json.dumps(body), headers=header)

        response = requests.get(url, data=json.dumps({}), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_get_random_item_wishlist_without_headers(self):
        body = {
            "email": "test_get_items_wishlist_without_headers@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post("http://localhost:3000/api/manage_wishlist/", data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 1",
            "item_name": "Hat",
            "description": "A red hat",
            "item_link": "www.amazon.com.br/red_hat",
            "picture": ""
        }
        _ = requests.post("http://localhost:3000/api/wishlist_items/", data=json.dumps(body), headers=header)

        url_params = url + f"?wishlist_name={body['wishlist_name']}"
        response = requests.get(url_params, data=json.dumps({}))
        self.assertEqual(response.status_code, 401)

    def test_get_random_item_wishlist_with_a_non_existing_wishlist(self):
        body = {
            "email": "test_get_items_wishlist_with_a_non_existing_wishlist@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post("http://localhost:3000/api/manage_wishlist/", data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 1",
            "item_name": "Hat",
            "description": "A red hat",
            "item_link": "www.amazon.com.br/red_hat",
            "picture": ""
        }
        _ = requests.post("http://localhost:3000/api/wishlist_items/", data=json.dumps(body), headers=header)

        url_params = url+f"?wishlist_name={'Non Existing WishList'}"
        response = requests.get(url_params, data=json.dumps({}), headers=header)
        self.assertEqual(response.status_code, 202)

    def test_get_random_item_wishlist_with_additional_fields_in_params(self):
        body = {
            "email": "test_get_items_wishlist_with_additional_fields_in_params@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post("http://localhost:3000/api/manage_wishlist/", data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 1",
            "item_name": "Hat",
            "description": "A red hat",
            "item_link": "www.amazon.com.br/red_hat",
            "picture": ""
        }
        _ = requests.post("http://localhost:3000/api/wishlist_items/", data=json.dumps(body), headers=header)

        url_params = url+f"?wishlist_name={body['wishlist_name']}&item_name={body['item_name']}"
        response = requests.get(url_params, data=json.dumps({}), headers=header)
        self.assertEqual(response.status_code, 200)
