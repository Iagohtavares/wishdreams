import json
import requests
import unittest

url = "http://localhost:3000/api/user/register/"


class TestUpdateCurrentUser(unittest.TestCase):

    def test_successful_update_current_user(self):
        body = {
            "email": "test_sucesseful_update_current_user@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body))
        response_login = response_login.json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "email": "test_sucesseful_update_current_user@test.com",
            "password": "12345",
            "name": "User Test",
            "username": "test_sucesseful_update_current_user",
            "phone": "11987654320"
        }
        response = requests.put(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_update_user_without_header(self):
        body = {
            "email": "test_update_user_without_header@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        _ = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body))

        body = {
            "email": "test_update_user_without_header@test.com",
            "password": "12345",
            "name": "User Test",
            "username": "user_test",
            "phone": "21987654321"
        }
        response = requests.put(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 401)

    def test_update_user_without_email(self):
        body = {
            "email": "test_update_user_without_email@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body))
        response_login = response_login.json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "password": "12345",
            "name": "User Test",
            "username": "user_test",
            "phone": "21987654321"
        }
        response = requests.put(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 500)

    def test_update_user_without_password(self):
        body = {
            "email": "test_update_user_without_password@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body))
        response_login = response_login.json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "email": "test_update_user_without_password@test.com",
            "name": "User Test",
            "username": "user_test",
            "phone": "21987654321"
        }
        response = requests.put(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 500)

    def test_update_user_without_some_fields(self):
        body = {
            "email": "test_update_user_without_some_fields@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body))
        response_login = response_login.json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "email": "test_update_user_without_some_fields@test.com",
            "password": "12345"
        }
        response = requests.put(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_update_user_email(self):
        body = {
            "email": "test_update_user_email@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body))
        response_login = response_login.json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "email": "test_update_user_email@test.com",
            "new_email": "new_test_update_user_email@test.com",
            "password": "12345"
        }
        response = requests.put(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_update_user_password(self):
        body = {
            "email": "test_update_user_password@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body))
        response_login = response_login.json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "email": "test_update_user_password@test.com",
            "password": "12345",
            "new_password": "123456789",
        }
        response = requests.put(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_update_current_user_with_additional_fields(self):
        body = {
            "email": "test_update_current_user_with_additional_fields@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body))
        response_login = response_login.json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "email": "test_update_current_user_with_additional_fields@test.com",
            "password": "12345",
            "name": "User Test",
            "username": "test_update_current_user_with_additional_fields_123",
            "phone": "21987654320",
            "New_field1": "Things1",
            "New_field2": [],
            "New_field3": {}
        }
        response = requests.put(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 200)
