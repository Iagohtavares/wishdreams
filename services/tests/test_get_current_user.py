import json
import requests
import unittest

url = "http://localhost:3000/api/user/register/"


class TestGetCurrentUser(unittest.TestCase):

    def test_successful_get_current_user(self):
        body = {
            "email": "test_sucesseful_get_current_user@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body))
        response_login = response_login.json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }
        response = requests.get(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_get_current_user_without_header(self):
        body = {
            "email": "test_get_current_user_without_header@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        _ = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body))

        response = requests.get(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 401)
