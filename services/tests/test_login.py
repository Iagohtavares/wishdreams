import json
import requests
import unittest

url = "http://localhost:3000/api/user/sign_in/"


class TestLogin(unittest.TestCase):

    def test_successful_login(self):
        body = {
            "email": "test_sucesseful_login@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 200)

    def test_login_without_email(self):
        body = {
            "email": "test_login_without_email@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))

        body = {
            "password": "12345",
        }
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 500)

    def test_login_with_wrong_email(self):
        body = {
            "email": "test_login_with_wrong_email@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))

        body.update({"email": "user@teste.com"})
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 400)

    def test_login_with_wrong_password(self):
        body = {
            "email": "test_login_with_wrong_password@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))

        body.update({"password": "123456"})
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 400)

    def test_login_without_password(self):
        body = {
            "email": "test_login_without_password@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))

        body = {
            "email": "test_login_without_password@test.com",
        }
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 500)

    def test_login_with_additional_fields(self):
        body = {
            "email": "test_login_with_additional_fields@test.com",
            "password": "12345",
            "New_field1": "Things1",
            "New_field2": [],
            "New_field3": {},
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 200)

