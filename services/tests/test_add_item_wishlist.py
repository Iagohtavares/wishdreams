import json
import requests
import unittest

url = "http://localhost:3000/api/wishlist_items/"


class TestAddItemWishlist(unittest.TestCase):

    def test_successful_add_item_wishlist(self):
        body = {
            "email": "test_successful_add_item_wishlist@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post("http://localhost:3000/api/manage_wishlist/", data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 1",
            "item_name": "Hat",
            "description": "A red hat",
            "item_link": "www.amazon.com.br/red_hat",
            "picture": ""
        }
        response = requests.post(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_add_item_wishlist_with_additional_fields(self):
        body = {
            "email": "test_add_item_wishlist_with_additional_fields@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post("http://localhost:3000/api/manage_wishlist/", data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 1",
            "item_name": "Hat",
            "description": "A red hat",
            "item_link": "www.amazon.com.br/red_hat",
            "picture": "",
            "item_buyed": False,
            "favorite": True
        }
        response = requests.post(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_add_item_wishlist_without_wishlist_name(self):
        body = {
            "email": "test_add_item_wishlist_without_wishlist_name@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post("http://localhost:3000/api/manage_wishlist/", data=json.dumps(body), headers=header)

        body = {
            "item_name": "Hat",
            "description": "A red hat",
            "item_link": "www.amazon.com.br/red_hat",
            "picture": "",
        }
        response = requests.post(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 500)

    def test_add_item_wishlist_with_a_non_existing_wishlist_name(self):
        body = {
            "email": "test_add_item_wishlist_with_a_non_existing_wishlist_name@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post("http://localhost:3000/api/manage_wishlist/", data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 100",
            "item_name": "Hat",
            "description": "A red hat",
            "item_link": "www.amazon.com.br/red_hat",
            "picture": "",
        }
        response = requests.post(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 400)

    def test_add_item_wishlist_with_wishlist_name_only(self):
        body = {
            "email": "test_add_item_wishlist_with_a_unique_field_wishlist_name@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post("http://localhost:3000/api/manage_wishlist/", data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 1"
        }
        response = requests.post(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 500)

    def test_add_item_wishlist_with_item_name_and_wishlist_name(self):
        body = {
            "email": "test_add_item_wishlist_with_a_unique_field_wishlist_name@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post("http://localhost:3000/api/manage_wishlist/", data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 1",
            "item_name": "Hat"
        }
        response = requests.post(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_add_item_wishlist_without_header(self):
        body = {
            "email": "test_add_item_wishlist_without_header@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post("http://localhost:3000/api/manage_wishlist/", data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 1",
            "item_name": "Hat",
            "description": "A red hat",
            "item_link": "www.amazon.com.br/red_hat",
            "picture": ""
        }
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 401)

    def test_add_a_repeat_item_in_wishlist(self):
        body = {
            "email": "test_add_a_repeat_item_in_wishlist@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post("http://localhost:3000/api/manage_wishlist/", data=json.dumps(body), headers=header)

        body = {
            "wishlist_name": "House List 1",
            "item_name": "Hat",
            "description": "A red hat",
            "item_link": "www.amazon.com.br/red_hat",
            "picture": ""
        }
        _ = requests.post(url, data=json.dumps(body), headers=header)
        response = requests.post(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 200)
