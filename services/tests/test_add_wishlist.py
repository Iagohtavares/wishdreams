import json
import requests
import unittest

url = "http://localhost:3000/api/manage_wishlist/"


class TestAddWishlist(unittest.TestCase):

    def test_successful_add_wishlist(self):
        body = {
            "email": "test_sucesseful_added_wishlist@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        response = requests.post(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_add_wishlist_with_additional_fields(self):
        body = {
            "email": "test_added_wishlist_with_additional_fields@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1",
            "New_field1": "Things1",
            "New_field2": [],
            "New_field3": {}
        }
        response = requests.post(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_add_wishlist_without_wishlist_name(self):
        body = {
            "email": "test_added_wishlist_without_wishlist_name@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        response = requests.post(url, data=json.dumps({}), headers=header)
        self.assertEqual(response.status_code, 500)

    def test_add_a_repeating_wishlist(self):
        body = {
            "email": "test_add_a_repeating_wishlist@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }

        body = {
            "wishlist_name": "House List 1"
        }
        _ = requests.post(url, data=json.dumps(body), headers=header)
        response = requests.post(url, data=json.dumps(body), headers=header)
        self.assertEqual(response.status_code, 202)

    def test_add_wishlist_without_header(self):
        body = {
            "email": "test_add_wishlist_without_header@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        _ = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body)).json()

        body = {
            "wishlist_name": "House List 1"
        }
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 401)
