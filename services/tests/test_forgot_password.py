import json
import requests
import unittest

url = "http://localhost:3000/api/user/forgot_password/"


class TestForgotPassword(unittest.TestCase):

    def test_successful_forgot_password(self):
        body = {
            "email": "test_sucesseful_forgot_password@test.com",
            "password": "12345"
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        _ = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body))
        _ = requests.post("http://localhost:3000/api/user/logout/", data=json.dumps(body))

        response = requests.post(url, data=json.dumps({"email": "test_sucesseful_forgot_password@test.com"}))
        self.assertEqual(response.status_code, 200)

    def test_forgot_password_without_email(self):
        response = requests.post(url, data=json.dumps({}))
        self.assertEqual(response.status_code, 500)

    def test_forgot_password_with_a_non_existing_email(self):
        body = {
            "email": "test_forgot_password_with_a_non_existing_email@test.com"
        }
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 400)

    def test_forgot_password_with_additional_fields(self):
        body = {
            "email": "test_forgot_password_with_additional_fields@test.com",
            "password": "12345"
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        _ = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body))
        _ = requests.post("http://localhost:3000/api/user/logout/", data=json.dumps(body))

        body = {
            "email": "test_forgot_password_with_additional_fields@test.com",
            "New_field1": "Things1",
            "New_field2": [],
            "New_field3": {}
        }
        response = requests.post(url, data=json.dumps(body))
        self.assertEqual(response.status_code, 200)
