import json
import requests
import unittest

url = "http://localhost:3000/api/user/logout/"


class TestLogout(unittest.TestCase):

    def test_successful_logout(self):
        body = {
            "email": "test_sucesseful_logout@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body))
        response_login = response_login.json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }
        response = requests.post(url, data=json.dumps({"email": "test_sucesseful_logout@test.com"}), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_logout_without_header(self):
        body = {
            "email": "test_logout_without_header@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))

        response = requests.post(url, data=json.dumps({"email": "test_logout_without_header@test.com"}))
        self.assertEqual(response.status_code, 401)

    def test_login_with_wrong_email(self):
        body = {
            "email": "test_login_with_wrong_email@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body))
        response_login = response_login.json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }
        response = requests.post(url, data=json.dumps({"email": "test_login_with_wrong_email@teste.com"}), headers=header)
        self.assertEqual(response.status_code, 400)

    def test_login_without_email(self):
        body = {
            "email": "test_login_without_email@test.com",
            "password": "12345",
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body))
        response_login = response_login.json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }
        response = requests.post(url, data=json.dumps({}), headers=header)
        self.assertEqual(response.status_code, 500)

    def test_logout_with_additional_fields(self):
        body = {
            "email": "test_logout_with_additional_fields@test.com",
            "password": "12345",
            "New_field1": "Things1",
            "New_field2": [],
            "New_field3": {},
        }
        _ = requests.post("http://localhost:3000/api/user/register/", data=json.dumps(body))
        response_login = requests.post("http://localhost:3000/api/user/sign_in/", data=json.dumps(body))
        response_login = response_login.json()

        header = {
            "authorization": f"Bearer {response_login['token']}"
        }
        response = requests.post(url, data=json.dumps({"email": "test_logout_with_additional_fields@test.com"}), headers=header)
        self.assertEqual(response.status_code, 200)
