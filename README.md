# API WishDreams API

Esta presente aplicação é uma API REST para um aplicativo de lista de desejos feito usando Python e MongoDB, 
carinhosamente apelidada de WishDreams. 

## Instalando (Necessário possuir Docker)

    docker-compose build

## Rodando o aplicativo

    docker-compose up -d

# API REST

A API está hospedada em `http://localhost:3000/`.
O aplicativo foi pensado para funcionar de maneira completa. Ou seja, para que o usuário tenha acesso às listas de desejo (criar uma lista, adicionar itens, etc) é necessário que ele esteja logado. Para que ele possa ter um login é necessário que ele tenha um cadastro.

## Fluxograma Simplicado da Aplicação

<img src="./docs/assets/fluxograma.png"/>


## Documentação de todas as API's
  https://app.swaggerhub.com/apis-docs/iagohtavares/WishDreams/1.0.0

Além disto, foi disponibilizado um arquivo .json na pasta docs, este arquivo pode ser importado como colection no postman e assim é possivel realizar os testes utilizando a ferramenta supracitada, caso queira.


## Autenticação

Todas as solicitações na API, exceto de inscrição do usuário, login do usuário, forgot password e reset password, exigem que o usuário esteja conectado. Para fazer isso, use o token do portador JWT recuperado no login do usuário como o método de autenticação para as solicitações.
