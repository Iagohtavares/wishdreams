from tornado.ioloop import IOLoop
from tornado.web import Application

from services.user import forgot_password, logout, register, reset_password, sign_in
from services.wishlist import manage_wishlist, wishlist_items, wishlist_random_item

items = []


def make_app():
    urls = [
        (r"/api/user/forgot_password/", forgot_password.ForgotPassword),
        (r"/api/user/logout/", logout.Logout),
        (r"/api/user/register/", register.Register),
        (r"/api/user/reset_password/", reset_password.ResetPassword),
        (r"/api/user/sign_in/", sign_in.SignIn),

        (r"/api/manage_wishlist/", manage_wishlist.ManageWishlist),
        (r"/api/wishlist_items/", wishlist_items.WishlistItems),
        (r"/api/wishlist_random_item/", wishlist_random_item.WishRandomItem),
    ]
    return Application(urls, debug=True)


if __name__ == '__main__':
    app = make_app()
    app.listen(3000)
    IOLoop.instance().start()
