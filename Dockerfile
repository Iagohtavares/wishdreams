FROM python:3.9
MAINTAINER Iago Henrique
RUN mkdir -p /var/www
WORKDIR /var/www
COPY . . /var/www
RUN pip install -r requirements.txt
CMD [ "python", "main.py" ]
EXPOSE 3000